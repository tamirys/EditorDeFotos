package model;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Tamirys
 */

//Botoes, suas instancias e foram adicionados no painel
public class PainelBotoes extends JPanel {

    private JButton btnCinza;
    private JButton btnVerde;
    private JButton btnAzul;
    private JButton btnVermelho;
    private JButton btnNegativo;

    private JButton btnRedimensionar;

    public PainelBotoes() {
        setLayout(new GridLayout(3, 2));
        btnCinza = new JButton("Aplicar cor cinza");
        btnVerde = new JButton("Aplicar cor verde");
        btnAzul = new JButton("Aplicar cor azul");
        btnVermelho = new JButton("Aplicar cor vermelho");
        btnNegativo = new JButton("Aplicar cor negativa");
        btnRedimensionar = new JButton("Redimensionar");

        add(btnCinza);
        add(btnVerde);
        add(btnAzul);
        add(btnVermelho);
        add(btnNegativo);
        add(btnRedimensionar);
    }

    public JButton getBtnCinza() {
        return btnCinza;
    }

    public void setBtnCinza(JButton btnCinza) {
        this.btnCinza = btnCinza;
    }

    public JButton getBtnVerde() {
        return btnVerde;
    }

    public void setBtnVerde(JButton btnVerde) {
        this.btnVerde = btnVerde;
    }

    public JButton getBtnAzul() {
        return btnAzul;
    }

    public void setBtnAzul(JButton btnAzul) {
        this.btnAzul = btnAzul;
    }

    public JButton getBtnVermelho() {
        return btnVermelho;
    }

    public void setBtnVermelho(JButton btnVermelho) {
        this.btnVermelho = btnVermelho;
    }

    public JButton getBtnNegativo() {
        return btnNegativo;
    }

    public void setBtnNegativo(JButton btnNegativo) {
        this.btnNegativo = btnNegativo;
    }

    public JButton getBtnRedimensionar() {
        return btnRedimensionar;
    }

    public void setBtnRedimensionar(JButton btnRedimensionar) {
        this.btnRedimensionar = btnRedimensionar;
    }
}
