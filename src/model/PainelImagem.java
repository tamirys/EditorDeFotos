package model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Tamirys
 */

//Pego a imagem, manipulo no ImageIO, depois a atualizo e desenho. Permitindo aplicar varios tons de cores.
//Redimensionando e limpando a imagem para nao redimensionar sobrescrever uma a outra. Redimensiono criando um array e um novo para ter o melhor controle do RGB que eu for manipular
public class PainelImagem extends JPanel {

    private BufferedImage bufferManipula;
    private File arquivo;

    public PainelImagem() {
        arquivo = new File("img.jpg");
        try {
            bufferManipula = ImageIO.read(arquivo);
        } catch (IOException ex) {
            Logger.getLogger(PainelImagem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizaImagem() {
        try {
            bufferManipula = ImageIO.read(arquivo);
        } catch (IOException ex) {
            Logger.getLogger(PainelImagem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(bufferManipula, 80, 80, null);

    }

    public void aplicarTonsCinza() {
        Color pixel;
        int media;

        for (int i = 0; i < bufferManipula.getWidth(); i++) {
            for (int j = 0; j < bufferManipula.getHeight(); j++) {
                pixel = new Color(bufferManipula.getRGB(i, j));
                media = (pixel.getRed() + pixel.getGreen() + pixel.getBlue()) / 3;
                bufferManipula.setRGB(i, j, new Color(media, media, media).getRGB());
            }
        }

        repaint();
    }

    public void aplicarTonsVermelhos() {
        Color pixel;

        for (int i = 0; i < bufferManipula.getWidth(); i++) {
            for (int j = 0; j < bufferManipula.getHeight(); j++) {
                pixel = new Color(bufferManipula.getRGB(i, j));
                bufferManipula.setRGB(i, j, new Color(pixel.getRed(), 0, 0).getRGB());

            }
        }

        repaint();

    }

    public void aplicarTonsVerde() {
        Color pixel;

        for (int i = 0; i < bufferManipula.getWidth(); i++) {
            for (int j = 0; j < bufferManipula.getHeight(); j++) {
                pixel = new Color(bufferManipula.getRGB(i, j));
                bufferManipula.setRGB(i, j, new Color(0, pixel.getGreen(), 0).getRGB());

            }
        }

        repaint();

    }

    public void aplicarTonsAzul() {
        Color pixel;

        for (int i = 0; i < bufferManipula.getWidth(); i++) {
            for (int j = 0; j < bufferManipula.getHeight(); j++) {
                pixel = new Color(bufferManipula.getRGB(i, j));
                bufferManipula.setRGB(i, j, new Color(0, 0, pixel.getBlue()).getRGB());

            }
        }

        repaint();

    }

    public void aplicarTonsNegativo() {
        Color pixel;

        for (int i = 0; i < bufferManipula.getWidth(); i++) {
            for (int j = 0; j < bufferManipula.getHeight(); j++) {
                pixel = new Color(bufferManipula.getRGB(i, j));
                bufferManipula.setRGB(i, j, new Color(255 - pixel.getRed(), 255 - pixel.getGreen(),
                        255 - pixel.getBlue()).getRGB());

            }
        }

        repaint();

    }

    public void redimisionar() {
        int[] arrayPixels;
        int[] novoArrayPixels;

        int altura = bufferManipula.getHeight();
        int largura = bufferManipula.getWidth();
        arrayPixels = bufferManipula.getRGB(0, 0, largura, altura, null, 0, largura);

        novoArrayPixels = new int[arrayPixels.length / 2];

        int j = 0;
        for (int i = 0; i < arrayPixels.length; i += 2) {
            novoArrayPixels[j] = arrayPixels[i];
            j++;
        }
        limpaImagem();
        bufferManipula.setRGB(0, 0, largura / 2, altura / 2, novoArrayPixels, 0, largura);
        repaint();
    }

    public void limpaImagem() {
        for (int i = 0; i < bufferManipula.getWidth(); i++) {
            for (int j = 0; j < bufferManipula.getHeight(); j++) {
                bufferManipula.setRGB(i, j, getBackground().getRGB());
            }
        }

    }
}
