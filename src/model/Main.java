package model;

import javax.swing.JFrame;

/**
 *
 * @author Tamirys
 */
public class Main {

    public static void main(String[] args) {
        Principal principal = new Principal();
        principal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        principal.setResizable(false);
        principal.setSize(800, 400);
        principal.setVisible(true);

    }
}
