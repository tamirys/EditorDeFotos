package model;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author Tamirys
 */
//parte visual, as action listeners dos buttons e da imagem, chamando os metodos e atualizando os mesmos.
public class Principal extends JFrame {

    private PainelImagem painelImagem;
    private PainelBotoes painelBotoes;

    public Principal() {
        setTitle("Editor de Foto");
        setLayout(new BorderLayout());
        painelImagem = new PainelImagem();
        painelBotoes = new PainelBotoes();

        add(painelImagem, BorderLayout.CENTER);
        add(painelBotoes, BorderLayout.EAST);

        painelBotoes.getBtnCinza().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                painelImagem.atualizaImagem();
                painelImagem.aplicarTonsCinza();
                repaint();
            }
        });
        painelBotoes.getBtnVermelho().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                painelImagem.atualizaImagem();
                painelImagem.aplicarTonsVermelhos();
                repaint();
            }
        });
        painelBotoes.getBtnVerde().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                painelImagem.atualizaImagem();
                painelImagem.aplicarTonsVerde();
                repaint();
            }
        });
        painelBotoes.getBtnAzul().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                painelImagem.atualizaImagem();
                painelImagem.aplicarTonsAzul();
                repaint();
            }
        });
        painelBotoes.getBtnNegativo().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                painelImagem.atualizaImagem();
                painelImagem.aplicarTonsNegativo();
                repaint();
            }
        });
        painelBotoes.getBtnRedimensionar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                painelImagem.redimisionar();
                repaint();
            }
        });

    }
 
}
